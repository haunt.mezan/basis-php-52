<?php
echo " Command line instructions
Git global setup

git config --global user.name \"Md Mezanur Rahaman\"
git config --global user.email \"haunt.mezan@outlook.com\"

Create a new repository

git clone https://gitlab.com/haunt.mezan/basis-php-52.git
cd basis-php-52
touch README.md
git add README.md
git commit -m \"add README\"
git push -u origin master

Existing folder

cd existing_folder
git init
git remote add origin https://gitlab.com/haunt.mezan/basis-php-52.git
git add .
git commit
git push -u origin master

Existing Git repository

cd existing_repo
git remote add origin https://gitlab.com/haunt.mezan/basis-php-52.git
git push -u origin --all
git push -u origin --tags";
?>